DOCUMENTO REQUISITI
- rifare schema prospetto per includere Amazon e l'utente amministratore
- scrittura scenari d'uso per amministrazione
- ri-sistemare obbiettivi funzionali di base
- rivedere assunzioni
- aggiungere & rianalizzare requisiti
+ negli scenari dare nomi sensati agli scenari, non numeri

ARCHITETTURA
- rivedere casi d'uso & aggiungere quelli relativi all'amministrazione
- ri-calcolo footprint cazziemmazzi

CODICE
- sistemare OCR
- costruire strumenti di amministrazione & analisi dei dati
- controllare gestore delle sessioni

TESTING
- pls kill me