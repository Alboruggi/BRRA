#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import requests
import cv2
from core.gestore_DB import *
from core.gestore_richieste import *

class TestRicercaPerImmagine(unittest.TestCase):
  
  def setUp(self):
    self.gestoreDB = GestoreDB()
    self.gestoreRichieste = GestoreRichieste(self.gestoreDB)
  
  def tearDown(self):
    del self.gestoreDB
    del self.gestoreRichieste
    try:
      os.remove("log.txt")
    except: pass
  
  def test_RI_1(self):
    img = cv2.imread("./core/Immagini/toobig.png")
    books = self.gestoreRichieste.elabora_immagine(img)
    self.assertEqual(books, [])
  
  def test_RI_2(self):
    img = cv2.imread("./core/Immagini/toosmall.png")
    books = self.gestoreRichieste.elabora_immagine(img)
    self.assertEqual(books, [])
  
  def test_RI_3(self):
    img = cv2.imread("./core/Immagini/notext.jpg")
    books = self.gestoreRichieste.elabora_immagine(img)
    self.assertEqual(books, [])
  
  def test_RI_4(self):
    img = cv2.imread("./core/Immagini/noinfo.jpg")
    books = self.gestoreRichieste.elabora_immagine(img)
    self.assertEqual(books, [])
  
  #QUESTO TEST FALLISCE PERCHE' LA FUNZIONE DI OCR HA SCARSE PRESTAZIONI IN GENERALE
  def test_RI_5(self):
    img = cv2.imread("./core/Immagini/test8.jpg")
    books = self.gestoreRichieste.elabora_immagine(img)
    self.assertNotEqual(books, [])


class TestRicercaPerTesto(unittest.TestCase):
  
  def setUp(self):
    self.gestoreDB = GestoreDB()
    self.gestoreRichieste = GestoreRichieste(self.gestoreDB)
  
  def tearDown(self):
    del self.gestoreDB
    del self.gestoreRichieste
    try:
      os.remove("log.txt")
    except: pass
  
  def test_RT_1(self):
    self.assertEqual(self.gestoreRichieste.ricerca_libri_naive(""), [])
  
  def test_RT_2(self):
    self.assertEqual(self.gestoreRichieste.ricerca_libri_naive("ahsya3yasd72jjauush"), [])
  
  def test_RT_3(self):
    self.assertNotEqual(self.gestoreRichieste.ricerca_libri_naive("Moby Dick"), [])
  
  def test_RT_4(self):
    self.assertNotEqual(self.gestoreRichieste.ricerca_libri_naive("Herman Melville"), [])


class TestCompraTitolo(unittest.TestCase):
  
  def check_url(self, url):
    request = requests.get(url)
    return request.status_code < 400 
  
  def setUp(self):
    self.gestoreDB = GestoreDB()
    self.gestoreRichieste = GestoreRichieste(self.gestoreDB)
  
  def tearDown(self):
    del self.gestoreDB
    del self.gestoreRichieste
    try:
      os.remove("log.txt")
    except: pass
  
  def test_CT_1(self):
    link = self.gestoreRichieste.compra_titolo("")
    self.assertIsNone(link)
  
  def test_CT_2(self):
    link = self.gestoreRichieste.compra_titolo(" ")
    self.assertIsNone(link)
  
  def test_CT_3(self):
    link = self.gestoreRichieste.compra_titolo(",")
    self.assertIsNone(link)
  
  def test_CT_4(self):
    link = self.gestoreRichieste.compra_titolo("?")
    self.assertIsNone(link)
  
  def test_CT_5(self):
    link = self.gestoreRichieste.compra_titolo("'")
    self.assertIsNone(link)
  
  def test_CT_6(self):
    link = self.gestoreRichieste.compra_titolo('"')
    self.assertIsNone(link)
  
  def test_CT_7(self):
    link = self.gestoreRichieste.compra_titolo("ò")
    self.assertTrue(self.check_url(link))
  
  def test_CT_8(self):
    link = self.gestoreRichieste.compra_titolo("I")
    self.assertTrue(self.check_url(link))
  
  def test_CT_9(self):
    link = self.gestoreRichieste.compra_titolo("Così è se vi pare")
    self.assertTrue(self.check_url(link))
  
  def test_CT_10(self):
    link = self.gestoreRichieste.compra_titolo("Il barone rampante")
    self.assertTrue(self.check_url(link))
  
  def test_CT_11(self):
    link = self.gestoreRichieste.compra_titolo("The knights who say 'ni'")
    self.assertTrue(self.check_url(link))
  
  def test_CT_12(self):
    link = self.gestoreRichieste.compra_titolo("Uno, nessuno e cento mila")
    self.assertTrue(self.check_url(link))
  
  def test_CT_13(self):
    link = self.gestoreRichieste.compra_titolo("Quo vadis?")
    self.assertTrue(self.check_url(link))
  
  def test_CT_13(self):
    link = self.gestoreRichieste.compra_titolo("Neuromancer")
    self.assertTrue(self.check_url(link))


if __name__ == "__main__":
  unittest.main()
