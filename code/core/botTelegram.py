#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from gestore_DB import *
from gestore_richieste import *
import logging
import cv2
import sys
import os
import signal
from threading import Thread
import threading

class BotBRRA(Thread):
    '''Gestore Telegram'''
    def __init__(self, gestoreSessioni):
        '''Inizializzazzione del bot telegram
        
        Args:
            gestoreSessioni	(gestoreSessioni):	gestoreSessoini

        Si connette a telegram e inizializza il servizio
        '''
        
        self.gestoreSessioni = gestoreSessioni
        Thread.__init__(self)


    def run(self):
        # Token bot telegram
        try:
          with open('Chiave_Telegram.txt') as f:
        	  lines = f.readline()
        except EnvironmentError:
          print("Errore nella lettura del token")
          print("Terminating...")
          os.kill(os.getpid(), signal.SIGUSR1)
        lines = lines.replace("\n","")
        self.updater = Updater(token=lines)
        self.dispatcher = self.updater.dispatcher
        # Enable logging
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s\
                            - %(message)s', level=logging.INFO)

        self.logger = logging.getLogger(__name__)
        self.msg_filter = Filters.private & (Filters.text | Filters.photo)
        self.start_handler = CommandHandler('start', self.start_t)
        self.help_handler  = CommandHandler('help', self.help)
        self.echo_handler  = MessageHandler(self.msg_filter, self.echo)
        self.dispatcher.add_handler(self.echo_handler)
        self.dispatcher.add_handler(self.start_handler)
        self.dispatcher.add_handler(self.help_handler)
        self.dispatcher.add_handler(CallbackQueryHandler(self.button))
        self.dispatcher.add_error_handler(self.error)
        self.updater.start_polling()
        self.updater.idle()
    
    
    def stop(self):
        '''Termina l'updater del bot Telegram'''
        self.updater.stop()
    
    
    def error(self,bot, update, error):
        '''Handler errori
        
        Args:
            bot(BotBRRA): bot telegram
            update(Updater): Updater di telegram
            Error(error): erroe riscontrato

        Comunica in shell l'errore
        '''
        
        self.logger.warning('Update "%s" caused error "%s"\n\n', update,
                            error)
    
    
    def start_t(self,bot, update):
        '''Messaggio di start per una nuova connessione
        
        Args:
            bot(BotBRRA): bot telegram
            update(Updater): Updater di telegram

        Quando un utente india il comando '\start' al bot, quest'ultimo
        gli comunica un messaggio di avvenuta connession
        '''
        
        bot.send_message(chat_id=update.message.chat_id,
                         text="Imma BOT! BRRABOT! Please talk to me!")
        self.scrivi_log("START"+"\n")


    def help(self,bot, update):
        '''Messaggio di help
        
        Args:
            bot(BotBRRA): bot telegram
            update(Updater): Updater di telegram

        Comunica le modalità con cui è possibile interagire con il bot
        telegram
        '''
        
        update.message.reply_text('Nobody can help you!')


    def echo(self,bot, update):
        '''Dispatcher richieste
        
        Args:
            bot(BotBRRA): bot telegram
            update(Updater): Updater di telegram

        Accoglie le richieste degli utenti e le smista alle rispettive
        classi che genereranno la risposta per l'utente
        '''
        
        if update.message:
            if update.message.photo:
                ## PARTE IMMAGINI
                photo_file = bot.get_file(update.message.photo[-1].file_id)
                photo_file.download(update.message.photo[-1].file_id)
                img = cv2.imread(update.message.photo[-1].file_id)
                self.scrivi_log("chat_id " + str(update.message.chat_id)
								+": Arrivata immagine");
                libri = self.gestoreSessioni.elabora_immagine(img)
                self.scrivi_log("chat_id " + str(update.message.chat_id)
								+ ": Elaborazione immagine")
                try:
                    os.remove(update.message.photo[-1].file_id)
                except: pass
            elif update.message: #Testo
                # Estraggo il testo
                msg = update.message.text
                if msg == "Classifica":
                    libri = self.gestoreSessioni.libri_in_classifica(3)
                else:
                    libri = self.gestoreSessioni.ricerca_libri_naive(msg,
                    update.message.chat_id)
            self.navigazione_risultati(bot,update, libri)


    def navigazione_risultati(self,bot,update, libri):
        '''Gestore della vista dei risultati
        
        Args:
            bot(BotBRRA): bot telegram
            update(Updater): Updater di telegram
            libri(list): lista di libro

        Genera l'interfaccia grafica per permettere all'utente di
        interagire con il servizio
        '''
        
        if len(libri) < 1:
            bot.send_message(chat_id=update.message.chat_id,
                             text="Spiacenti, la ricerca non ha \
                                   dato corrispondenze")
            self.scrivi_log("chat_id " + str(update.message.chat_id) +
                            ": Libro non trovato")
        self.scrivi_log("chat_id " + str(update.message.chat_id) +
                        ": Creazione descrizioni")
        self.scrivi_log("chat_id " + str(update.message.chat_id) +
                        ": Tre bottoni scelta continua a leggere, altro dell'autore ecc")
        for libro in libri:
            messaggio = self.gestoreSessioni.crea_descrizione(libro,100)
            keyboard = [[InlineKeyboardButton("Continua a leggere",
                            callback_data = 't'+libro[0]),
                            InlineKeyboardButton("Altro dall'autore",
                            callback_data = 'a'+libro[1])],
                        [InlineKeyboardButton("Link di acquisto",
                            url = self.gestoreSessioni.compra_titolo(libro[0]))] ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            bot.send_message(chat_id=update.message.chat_id,text=messaggio,
							reply_markup=reply_markup)


    def button(self,bot, update):
        '''Crea la tastiera in chat
        
        Args:
            bot(BotBRRA): bot telegram
            update(Updater): Updater di telegram

        Genera i bottoni per la navigazione di ogni singolo libro
        '''
        
        query = update.callback_query
        data = format(query.data)
        if(data[0] == "t"):
            titolo = data[1:]
            libri = self.gestoreSessioni.ricerca_libro(titolo)
            self.scrivi_log("chat_id " + str(query.message.chat_id) +
                            ": Ricerca del libro")
            for libro in libri:
                keyboard = [[InlineKeyboardButton("Altro dall'autore",
                                callback_data='a'+ libro[1])],
                            [InlineKeyboardButton("Link di acquisto",
                                url = self.gestoreSessioni.compra_titolo(libro[0]))] ]
                reply_markup = InlineKeyboardMarkup(keyboard)
                testo = self.gestoreSessioni.crea_descrizione(libro,-1)
                bot.edit_message_text(text=testo, chat_id=query.message.chat_id,
                                      message_id=query.message.message_id,
                                      reply_markup=reply_markup)
        else:
            autore = data[1:]
            libri = self.gestoreSessioni.ricerca_libri(autore,"","",
												query.message.chat_id)
            self.scrivi_log("chat_id " + str(query.message.chat_id) +
                            ": Ricerco libro")
            self.navigazione_risultati(bot,query, libri)
            self.scrivi_log("chat_id "+ str(query.message.chat_id) +
                            ": Navigazione risultati")
    
    def scrivi_log(self,nome):
        '''Scrive nel file di log
        
        Args:
            nome (str): frase da scrivere
        
        Prende in ingresso una stringa e la scrive nel file di log 
        '''
        
        with open("log.txt", "a") as log:
            log.write(nome+"\n")


if __name__ == "__main__":
    BotBRRA()
    open("log.txt", "w").close() 												
