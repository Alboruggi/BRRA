#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import cv2
from rdflib        import Graph, URIRef
from SPARQLWrapper import SPARQLWrapper, JSON
from urllib2       import quote
from PIL           import Image


class GestoreDB:
    
    def __init__(self):
		'''Inizializza GestoreDB

        Args:
            none

        Returns:
            none
		
		Inizializza la connesione allo SPARQL endpoint, il knowledge graph e 
		i database.
		'''	
        	
		self.sparql = SPARQLWrapper("http://dbpedia.org/sparql")
		self.sparql.setReturnFormat(JSON)
		self.kg = Graph()
		self.cronologia = dict(titolo=[],utente=[])
		self.classifica = []
		self.utenti = []

    def ricerca_per_titolo(self,titolo):
        '''Genera la query di ricerca dato il titolo del libro

        Args:
            titolo (string): Titolo del libro da cercare

        Returns:
            libri (libro): lista di libri

        Dato il titolo di un libro crea una query in formato SPARQL e la esegue
        chiamando la funzione apposita restituendo i libri trovati dalla query
        '''
        
        query = """
                    PREFIX dbo: <http://dbpedia.org/ontology/>
                    SELECT ?titolo ?nomeAutore ?abstract ?book ?autore WHERE {
                    ?book  a               <http://dbpedia.org/ontology/Book>;
                           rdfs:label      \""""+titolo+"""\"@it;
                           rdfs:label      ?titolo;
                           dbo:abstract    ?abstract;
                           dbo:author      ?autore.
                    ?autore rdfs:label      ?nomeAutore.

                    FILTER (lang(?abstract) = 'it')
                    FILTER (lang(?nomeAutore) = 'it')
                    FILTER (lang(?titolo) = 'it')
                    }
                """
        return self.esegui_query(query,0)
    
    
    def ricerca_per_autore(self,nomeIntero,cognome):
        '''Genera la quey di ricerca dato l'autore

        Args:
            nomeIntero (string): Nome e Cognome dell'autore
            cognome (string): Cognome dell'autore

        Returns:
            libri (libro): lista di libri

        Dato il nome e cognome o solo il congnome dell'autore di un libro crea
        una query in formato SPARQL e la esegue chiamando la funzione apposita
        restituendo i libri trovati dalla query
        '''
        
        if len(nomeIntero) > 0:                                                  # Caso in cui ho nome e cognome assieme
            query = """
                        PREFIX dbo: <http://dbpedia.org/ontology/>
                        PREFIX foaf:  <http://xmlns.com/foaf/0.1/>
                        SELECT  DISTINCT ?titolo ?nomeAutore ?abstract
                                         ?book ?autore {
                        ?autore foaf:name   \""""+nomeIntero+"""\"@en;
                                rdfs:label     ?nomeAutore.
                        ?book   a           <http://dbpedia.org/ontology/Book>;
                                rdfs:label   ?titolo;
                                dbo:abstract    ?abstract;
                                dbo:author      ?autore.
                        FILTER (lang(?abstract) = 'it')
                        FILTER (lang(?titolo) = 'it')
                        FILTER (lang(?nomeAutore) = 'it')
                        FILTER (lang(?titolo) = 'it')
                        } LIMIT 4
                    """
        else:                                                                   # Caso in cui ho solo il cognome
            query = """
                        PREFIX dbo: <http://dbpedia.org/ontology/>
                        PREFIX foaf:  <http://xmlns.com/foaf/0.1/>
                        SELECT  DISTINCT ?titolo ?nomeAutore
                                         ?abstract ?book ?autore {
                        ?autore foaf:surname \""""+cognome+"""\"@en;
                                rdfs:label  ?nomeAutore.
                        ?book   a           <http://dbpedia.org/ontology/Book>;
                                rdfs:label  ?titolo;
                                dbo:abstract    ?abstract;
                                dbo:author      ?autore.
                        FILTER (lang(?abstract) = 'it')
                        FILTER (lang(?titolo) = 'it')
                        FILTER (lang(?nomeAutore) = 'it')
                        FILTER (lang(?titolo) = 'it')
                        } LIMIT 4
                    """
        return self.esegui_query(query,4)
    
    
    def cerca_titoli_simili(self,titolo):
        '''Genera la quey di ricerca di titoli simili al titolo del libro dato
        in input

        Args:
            titolo (string): Titolo del libro da cercare

        Returns:
            libri (libro): lista di libri

        Dato il il titolo di un libro genera ed eseque tramite funzione
        apposita la query per trovare titoli simili a quello di input, i libri
        simili si intendono aventi lo stesso genere
        '''
        
        query = """                                                     		# Trova titoli simili per genere
                    PREFIX dbo: <http://dbpedia.org/ontology/>
                    SELECT DISTINCT ?titolo ?nomeAutore ?abstract
                                    ?book ?autore  WHERE {
                    ?bookq  a      <http://dbpedia.org/ontology/Book>;
                            rdfs:label      \"""" + titolo + """\"@it;
                            dbo:literaryGenre ?genereq.
                    ?book   a              <http://dbpedia.org/ontology/Book>;
                            dbo:literaryGenre ?genereq;
                            rdfs:label      ?titolo;
                            dbo:abstract    ?abstract;
                            dbo:author      ?autore.
                            ?autore rdfs:label      ?nomeAutore.
                    FILTER (lang(?abstract) = 'it')
                    FILTER (lang(?nomeAutore) = 'it')
                    FILTER (lang(?titolo) = 'it')
                    } LIMIT 4
                """
        return self.esegui_query(query,4)
    
    
    def esegui_query(self,query,minimoLibri):
        '''Esegue una query

        Args:
            query (string): query da eseguire
            minimoLibri (int): numero minimo di libri che la query deve
                               restituire
        Returns:
            libri (libro): lista di libri

        Data una query la esegue prima sul knowledge graph (database 
        interno), se non trova nessun risultato, o se il numero di libri
        trovato è inferiore a quello minimo richiesto allora la query 
        viene eseguita su dbpedia, in questa circostanza il knowledge 
        graph viene aggiornato con i libri appena trovati.
        '''
        
        libri = []
        qres = self.kg.query(query)                                     		# interrogo KG interno
        found = 0
        if len(qres) > minimoLibri:
            for row in qres:
                libri.append(row)
            found = 1
        else:                                                          			# se non trovo il libro nel KG interno
            qres = self.sparql.setQuery(query)
            results = self.sparql.query().convert()
            for result in results["results"]["bindings"]:
                autore   = result["nomeAutore"]["value"]
                abstract = result["abstract"]["value"]
                resBook  = result["book"]["value"]
                resAuth  = result["autore"]["value"]
                titolo   = result["titolo"]["value"]
                found = 1
                libri.append([titolo, autore, abstract, resBook, resAuth])
                self.kg.parse(quote(resAuth.encode('utf-8'),safe=':/')) 		# Aggiorno il KG interno
                self.kg.parse(quote(resBook.encode('utf-8'),safe=':/'))
        return libri
    
    
    def aggiorna_cronologia(self,titolo,id_utente):
        '''Aggiorna la cronologia

        Args:
            titolo (string): titolo del libro
            id_utente (string): id utente

        Inserisce il titolo nella classifica o se già presente ne 
        aggiorna la frequenza
        '''
        
        self.cronologia['titolo'].append(titolo)
        self.cronologia['utente'].append(id_utente)
        self.inserisci_utente(id_utente)
    
    
    def inserisci_utente(self,id_utente):
        '''Aggiungo un utente al db degli utenti

        Args:
            id_utente (string): id utente
        
        Controlla se un utente non è gia registrato, nel caso 		           #TODO: Modificato
        non sia registrato lo aggiunge altrimenti non fa nulla
        '''
        
        if id_utente not in self.utenti:
            self.utenti.append(id_utente)
    
    
    def aggiorna_classifica(self):
        '''Aggiorna la classifica dei libri'''
        classifica = {i:self.cronologia['titolo'].count(i)
                            for i in self.cronologia['titolo']}
        titoli = []
        frequenza = []
        for i,j in classifica.items():
            titoli.append(i)
            frequenza.append(j)

        self.classifica = [x for _,x in sorted(zip(frequenza,titoli),
                                               reverse=True)]



















'''
##########-------------------------------##########
###                                             ###
###                   TESTING                   ###
###                                             ###
##########-------------------------------##########
if __name__ == "__main__":


    gestoreKG       = GestoreDB()
    gestoreSessioni = GestoreRichieste(gestoreKG)



    gestoreSessioni.ricerca_libri_naive("La mala ora")


    gestoreSessioni.ricerca_libri_naive("Seta")
    gestoreSessioni.ricerca_libri_naive("Seta")

    gestoreSessioni.ricerca_libri_naive("Moby Dick")
    gestoreSessioni.ricerca_libri_naive("Moby Dick")
    gestoreSessioni.ricerca_libri_naive("Moby Dick")
    gestoreSessioni.ricerca_libri_naive("Moby Dick")
    gestoreSessioni.ricerca_libri_naive("I promessi sposi")

    gestoreSessioni.libri_in_classifica(3)

    #a = gestoreSessioni.ricerca_libri("","","Moby Dick")
    #print(type(a))
    #print(len(a))
    #gestoreSessioni.ricerca_libri("","Melville","")
    #gestoreSessioni.ricerca_libri("Irvine Welsh","","")
    #gestoreSessioni.ricercaPerInformazione("","","Moby Dick")
    #print(gestoreSessioni.compra_titolo("I promessi sposi"))

    #a = gestoreKG.cerca_titoli_simili("Moby Dick")
    #print(a)
    #print(gestoreKG.ricerca_per_titolo("La mala ora")[0][0])
    #print(gestoreKG.ricerca_per_titolo("La mala ora")[0][0])

    #gestoreKG.ricerca_per_titolo("Una pagina d'amore")
'''
