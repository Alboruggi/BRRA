#!/usr/bin/env python
# -*- coding: utf-8 -*-

from gestore_amministrazione import *
from gestore_DB import *
from gestore_richieste import *
from botTelegram import *

from threading  import Thread
from random     import randint
from cmd        import Cmd
import time
import os

class BRRAPrompt(Cmd):
    
    def __init__(self,gestoreAmministrazione,bot):
      '''Si occupa inizializzare la classe BRRAPrompt

          Args:																	
              gestoreAmministrazione(gestoreAmministrazione):	gestoreAmministrazione
              bot(BotBRRA): bot telegram
          
          Si occupa di far par partire il bot BRRA e nello stesso tempo permette 
          di interagire all'amministtratore da console.
          Permette di visualizzare statistiche, eseguire backup, caricare bakcup
          '''

      Cmd.__init__(self)
      Cmd.default = self.default_msg
      self.gestoreAmministrazione = gestoreAmministrazione
      self.bot = bot
      print('Welcome in BRRA prompt, where magic happens...')
      print("                 ______  _______    _______         _       \n" +
            "                |_   _ \|_   __ \  |_   __ \       / \      \n" +
            "                  | |_) | | |__) |   | |__) |     / _ \     \n" +
            "                  |  __'. |  __ /    |  __ /     / ___ \    \n" +
            "                 _| |__) || |  \ \_ _| |  \ \_ _/ /   \ \_  \n" +
            "                |_______/____| |___|____| |___|____| |____| \n" )
      self.do_help(None)
      print('\n\n\n\n\n\n\n\n\n\n')
    
    
    def default_msg(self, line):
      '''Messaggio standard per comandi non riconosciuti'''
      print('%s is not a valid command. Use "help" for a list of commands'%(line))
    
    
    def do_help(self, args):
      '''Stampa l'help dei comandi'''
      print('Available commands:')
      print('quit   - quit the program')
      print('stat   - see the statistics')
      print('backup - create backups')
      print('load   - load the bakup files')
      print('help   - print this help')
    
    
    def do_stat(self, args):
      '''Si occupa di far partire visualizza statistiche
      
      Richiama la funzione visualizza_statistiche di gestore 
      amministrazione, le statistiche riportate sono il numero di utenti, le 
      ricerche effettuate, il totale dei libri
      '''
          
      self.gestoreAmministrazione.visualizza_statistiche()
    
    
    def do_backup(self, args):
      '''Backup manuale delle basi di dati
      
      Richiama la funzione backup_manuale della classe GestoreAmministrazione
      che si occupa di fare il backup di:
        - knowledge_graph.bak il backup del grafo
        - utenti.bak il backup degli utenti
        - cronologia.bak il backup delle ricerche
      '''
      
      self.gestoreAmministrazione.backup_manuale()
    
    
    def do_load(self, args):
      '''Ripristina basi di dati da file di backup
      
      Si occupa di richiamare la funzione ripristina_backup della 
      della classe gestoreAmministrazione, ripristina i seguenti bakcup:  
        - knowledge_graph.bak il backup del grafo
        - utenti.bak il backup degli utenti
        - cronologia.bak il backup delle ricerche
      '''
      
      self.gestoreAmministrazione.ripristina_backup()
    
    
    def do_quit(self, args):
      '''Termina il programma'''
      print "Exiting..."
      self.bot.stop()
      print "Done."
      os.kill(os.getpid(), signal.SIGUSR1)



if __name__ == "__main__":
    config = 'config.BRRA'
    if os.path.isfile(config):
        with open(config) as f:
    		backup_dir = f.readline()[:-1]
    else:
        backup_dir = str(raw_input("Inserisci il percorso in cui vuoi " +
                                   "salvare i file di backup: "))
        if backup_dir[-1] != '/':
            backup_dir = backup_dir + '/'
        with open(config, "w") as file:
            file.write(backup_dir + "\n")

    gestoreDB = GestoreDB()
    gestoreRichieste = GestoreRichieste(gestoreDB)
    gestoreAmministrazione = GestoreAmministrazione(gestoreDB, backup_dir)
    bot = BotBRRA(gestoreRichieste)
    bot.start()
    gestoreAmministrazione.start()
    time.sleep(1)
    prompt = BRRAPrompt(gestoreAmministrazione, bot)
    prompt.prompt = '> '
    try:
      prompt.cmdloop()
    except:
        print "Ops! Something went horribly wrong... \nExiting."
        prompt.do_quit()
