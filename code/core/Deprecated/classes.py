#!/usr/bin/env python
# -*- coding: utf-8 -*-


import pytesseract
import argparse
import cv2
from rdflib        import Graph, URIRef
from SPARQLWrapper import SPARQLWrapper, JSON
from urllib2 import quote
from PIL import Image

class gestoreDelleSessioni:

    def __init__(self,gestoreDelKG):
        '''Inizializzazzione della classe gestoreDelleSessioni

        Args:
            gestoreDelKG (gestoreDelKG): Classe che si occupa di gestire il KG

        Returns:
            True

        Istanzia il gestore del KG, la classifica e il rank della classifica.

        classifica (list): contiene i titoli ricercati
        rank (list): contiene le frequenze dei titoli ricercati
        '''
        self.gestoreDelKG = gestoreDelKG
        self.classifica = []
        self.rank = []

        return True


    def elaboraImmagine(self, img):
        '''Elaborazione delle immagini

        Args:
            img (img): Immagine da analizzare

        Returns:
            libri (libro): lista di libri inerenti al libro presente in img

        Elabora l'immagine cercandone il testo
        '''
        testo = pytesseract.image_to_string(img)

        # TODO: elaborazione del testo
        # TODO: integrare parte biondo
        # TODO: restituire libri
        # libri = self.ricercaLibri("","",titolo)
        return "ciao"


    def ricercaLibriNaive(self,testo):
        '''Ricerca libri in modo naive

        Args:
            testo (string): testo che può essere il titolo del libro, il
                            il cognome dell'autore o il nome completo
                            dell'autore

        Returns:
            libri (libro): lista di libri inerenti al libro da cercare

        Approccio a tentativi: prima cerca il libro come se il testo fosse il
        titolo del libro, se restituisce un libro allora cerco titoli simili da
        consigliare. Altrimenti provo cercando libri per nome completo
        dell'autore o solo cognome.
        '''
        libri = self.gestoreDelKG.ricercaPerTitolo(testo)

        if len(libri) > 0:
            self.aggiungiLibroInClassifica(testo)
            libri.extend(self.gestoreDelKG.cercaTitoliSimili(testo))

        if len(libri) < 1:
            libri = self.gestoreDelKG.ricercaPerAutore(testo,"")

        if len(libri) < 1:
            libri = self.gestoreDelKG.ricercaPerAutore("",testo)

        return libri


    def ricercaLibri(self,nomeIntero,cognome,titolo):
        '''Ricerca libri ottimizzata

        Args:
            nomeIntero (string): nome e cognome dell'autore
            cognome (string): cognome dell'autore
            titolo (string): titolo del libro

        Returns:
            libri (libro): lista di libri inerenti al libro da cercare/autore

        Cerca i titolo in base alle informazioni a disposizione. Avere
        nomeItero o cognome è indifferete, avere il nome dell'autore esclude la
        ricerca per titolo.
        '''
        if(len(cognome)>0 or len(nomeIntero)>0):
            libri = self.gestoreDelKG.ricercaPerAutore(nomeIntero,cognome)
        else:
            libri = self.gestoreDelKG.ricercaPerTitolo(titolo)
            if libri is not None:
                self.aggiungiLibroInClassifica(titolo)
                libri.extend(self.gestoreDelKG.cercaTitoliSimili(titolo))
        return libri


    def ricercaLibro(self,titolo):
        '''Ricerca libro

        Args:
            titolo (string): titolo del libro

        Returns:
            libro (libro): libro cercato

        Cerca il titolo ricercato
        '''
        return  self.gestoreDelKG.ricercaPerTitolo(titolo)


    def compraTitolo(self,titolo):
        '''Compra titolo

        Args:
            titolo (string): titolo del libro

        Returns:
            link (string): link di acquisto generato

        Crea il link di acquisto per il titolo desiderato.
        '''
        titolo = quote(titolo.encode('utf-8'),safe=':/')
        link = """https://www.amazon.it/s/ref=nb_sb_ss_c_1_14/258-9929431-02617
                  15?__mk_it_IT=ÅMÅŽÕÑ&url=search-alias%3Daps&field-keywords="""
        link = link + titolo.replace(" ","+")
        return link


    def aggiungiLibroInClassifica(self,titolo):
        '''Aggiunge libro in classifica

        Args:
            titolo (string): titolo del libro

        Returns:
            true

        Inserisce il titolo nella classifica o se già presente ne aggiorna la
        frequenza
        '''
        if titolo not in self.classifica:
            self.classifica.append(titolo)
            self.rank.append(1)
        else:
            pos = self.classifica.index(titolo)
            self.rank[pos] += 1
        return True

    def libriInClassifica(self,nItem):
        '''Visualizza i libri in classifica

        Args:
            nItem (int): numero di titoli da visualizzare

        Returns:
            libri (libro): lista di libri nelle prime posizioni in classifica

        Restituisce i primi nItem libri in classifica aggiornando le varie
        liste ordinandole per frequenza
        '''
        self.classifica = [self.classifica for _,self.classifica
                           in sorted(zip(self.rank,self.classifica),
                           reverse=True)]
        self.rank = sorted(self.rank, reverse=True)

        libri = self.gestoreDelKG.ricercaPerTitolo(self.classifica[0])
        for i in range(1,nItem):
            libri.extend(self.gestoreDelKG.ricercaPerTitolo(
                                                  self.classifica[i]))

        return libri






class gestoreDelKG:                                                             #KGB per gli amici
    def __init__(self):
        self.sparql = SPARQLWrapper("http://dbpedia.org/sparql")
        self.sparql.setReturnFormat(JSON)
        self.kg = Graph()

    def ricercaPerTitolo(self,titolo):
        # Generazione della query
        query = """
                    PREFIX dbo: <http://dbpedia.org/ontology/>
                    SELECT ?titolo ?nomeAutore ?abstract ?book ?autore WHERE {
                    ?book  a               <http://dbpedia.org/ontology/Book>;
                           rdfs:label      \""""+titolo+"""\"@it;
                           rdfs:label      ?titolo;
                           dbo:abstract    ?abstract;
                           dbo:author      ?autore.
                    ?autore rdfs:label      ?nomeAutore.

                    FILTER (lang(?abstract) = 'it')
                    FILTER (lang(?nomeAutore) = 'it')
                    FILTER (lang(?titolo) = 'it')
                    }
                """
        return self.eseguiQuery(query,0)

    def ricercaPerAutore(self,nomeIntero,cognome):
        # TODO: ERRORE SCONOSCIUTO CON IL NOME, SU VIRTUOSO LA QUERY FUNZIONA
        # Generazione della query
        if(len(nomeIntero)>0):
            # Caso in cui ho nome e cognome assieme

            query = """
                        PREFIX dbo: <http://dbpedia.org/ontology/>
                        PREFIX foaf:  <http://xmlns.com/foaf/0.1/>
                        SELECT  DISTINCT ?titolo ?nomeAutore ?abstract
                                         ?book ?autore {
                        ?autore foaf:name   \""""+nomeIntero+"""\"@en;
                                rdfs:label     ?nomeAutore.
                        ?book   a           <http://dbpedia.org/ontology/Book>;
                                rdfs:label   ?titolo;
                                dbo:abstract    ?abstract;
                                dbo:author      ?autore.
                        FILTER (lang(?abstract) = 'it')
                        FILTER (lang(?titolo) = 'it')
                        FILTER (lang(?nomeAutore) = 'it')
                        FILTER (lang(?titolo) = 'it')
                        } LIMIT 4
                    """
        else:
            # Caso in cui ho solo il cognome
            query = """
                        PREFIX dbo: <http://dbpedia.org/ontology/>
                        PREFIX foaf:  <http://xmlns.com/foaf/0.1/>
                        SELECT  DISTINCT ?titolo ?nomeAutore
                                         ?abstract ?book ?autore {
                        ?autore foaf:surname \""""+cognome+"""\"@en;
                                rdfs:label  ?nomeAutore.
                        ?book   a           <http://dbpedia.org/ontology/Book>;
                                rdfs:label  ?titolo;
                                dbo:abstract    ?abstract;
                                dbo:author      ?autore.
                        FILTER (lang(?abstract) = 'it')
                        FILTER (lang(?titolo) = 'it')
                        FILTER (lang(?nomeAutore) = 'it')
                        FILTER (lang(?titolo) = 'it')
                        } LIMIT 4
                    """

        return self.eseguiQuery(query,4)




    def cercaTitoliSimili(self,titolo):
        # Trova titoli simili per genere
        query = """
                    PREFIX dbo: <http://dbpedia.org/ontology/>
                    SELECT DISTINCT ?titolo ?nomeAutore ?abstract
                                    ?book ?autore  WHERE {
                    ?bookq  a      <http://dbpedia.org/ontology/Book>;
                            rdfs:label      \"""" + titolo + """\"@it;
                            dbo:literaryGenre ?genereq.
                    ?book   a              <http://dbpedia.org/ontology/Book>;
                            dbo:literaryGenre ?genereq;
                            rdfs:label      ?titolo;
                            dbo:abstract    ?abstract;
                            dbo:author      ?autore.
                            ?autore rdfs:label      ?nomeAutore.
                    FILTER (lang(?abstract) = 'it')
                    FILTER (lang(?nomeAutore) = 'it')
                    FILTER (lang(?titolo) = 'it')
                    } LIMIT 4
                """
        return self.eseguiQuery(query,4)



    def eseguiQuery(self,query,minimoLibri):
        libri = []
        #Interrogo KG interno
        qres = self.kg.query(query)
        found = 0

        if(len(qres)>minimoLibri):
            #print('kg interno')
            for row in qres:
                libri.append(row)
            found = 1

        else: # Se non trovo il libro nel KG interno
            #print('kg esterno')
            qres = self.sparql.setQuery(query)
            results = self.sparql.query().convert()

            for result in results["results"]["bindings"]:
                autore      = result["nomeAutore"]["value"]
                abstract    = result["abstract"]["value"]
                resBook     = result["book"]["value"]
                resAuth     = result["autore"]["value"]
                titolo      = result["titolo"]["value"]
                found = 1
                libri.append([titolo, autore, abstract, resBook, resAuth])

                # Aggiorno il KG interno
                self.kg.parse(quote(resAuth.encode('utf-8'),safe=':/'))
                self.kg.parse(quote(resBook.encode('utf-8'),safe=':/'))

        # Print Libri
        '''
        if(found == 1):
            for libro in libri:
                print('Titolo:      %s'   % libro[0])
                print('Autore:      %s'   % libro[1])
                print('Commento:    %s \n'% "...")#libro[2]
        else:
            print('Nessun libro trovato')
        '''
        return libri




'''
##########-------------------------------##########
###                                             ###
###                   TESTING                   ###
###                                             ###
##########-------------------------------##########
if __name__ == "__main__":


    gestoreKG       = gestoreDelKG()
    gestoreSessioni = gestoreDelleSessioni(gestoreKG)



    gestoreSessioni.ricercaLibriNaive("La mala ora")


    gestoreSessioni.ricercaLibriNaive("Seta")
    gestoreSessioni.ricercaLibriNaive("Seta")

    gestoreSessioni.ricercaLibriNaive("Moby Dick")
    gestoreSessioni.ricercaLibriNaive("Moby Dick")
    gestoreSessioni.ricercaLibriNaive("Moby Dick")
    gestoreSessioni.ricercaLibriNaive("Moby Dick")
    gestoreSessioni.ricercaLibriNaive("I promessi sposi")

    gestoreSessioni.libriInClassifica(3)

    #a = gestoreSessioni.ricercaLibri("","","Moby Dick")
    #print(type(a))
    #print(len(a))
    #gestoreSessioni.ricercaLibri("","Melville","")
    #gestoreSessioni.ricercaLibri("Irvine Welsh","","")
    #gestoreSessioni.ricercaPerInformazione("","","Moby Dick")
    #print(gestoreSessioni.compraTitolo("I promessi sposi"))

    #a = gestoreKG.cercaTitoliSimili("Moby Dick")
    #print(a)
    #print(gestoreKG.ricercaPerTitolo("La mala ora")[0][0])
    #print(gestoreKG.ricercaPerTitolo("La mala ora")[0][0])

    #gestoreKG.ricercaPerTitolo("Una pagina d'amore")
'''
