import re
import cv2
import nltk
import numpy as np
import pytesseract
from scipy import ndimage
from Levenshtein import distance

MEDIAN_NUMBER = 5;

def ocr(img):
	libro = "";

	print('PRIMA lettura iniziale')
	testo = pytesseract.image_to_string(img,lang ='ita')
	testo = pulizia(testo)+' '
	libro+=testo;

	print('SECONDA lettura dopo aver applicato una gaussiano')
	img2 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv2.THRESH_BINARY,11,2)
	testo = pytesseract.image_to_string(img2,lang ='ita')
	testo = pulizia(testo)+ ''
	libro+=testo;

	print('TERZA dopo aver applicato erosione e inversione maschera')
	im3 = ndimage.binary_erosion(img2, structure=np.ones((5,3))).astype(np.float32)
	im3 = ndimage.binary_dilation(im3, structure=np.ones((5,3))).astype(np.float32)
	im3 = 1-im3;
	im3 = cv2.medianBlur(im3, MEDIAN_NUMBER)
	testo = pytesseract.image_to_string(im3)
	testo = pulizia(testo)+''
	libro+=testo
	return str(libro);

def pulizia(libro):
	libro = re.sub(r'[\n \t]+',' ', libro);
	libro = re.sub(r'[^A-Z a-z 0-9 \']+','', libro);
	return libro;

#Main
if __name__ == "__main__":

	libro = "";
	img = cv2.imread('test.jpg',0)
	libro = ocr(img)
	print("\n\nTitolo del libro:")
	libro = libro.lower()
	print(libro);

	A = "Oscar Moderni Gabriel Garcia Marquez l'amore ai tempi del colera".lower()
	B = "Alessandro Baricco  Novecento un monologo".lower()
	C = "Alessandro Baricco  Castelli di rabbia".lower()

	print(distance(libro,A)-abs(len(libro)-len(A)))
	print(distance(libro,B)-abs(len(libro)-len(B)))
	print(distance(libro,C)-abs(len(libro)-len(C)))