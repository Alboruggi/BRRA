#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import cv2
from rdflib        import Graph, URIRef
from SPARQLWrapper import SPARQLWrapper, JSON
from urllib2 import quote
from PIL import Image
import pytesseract
from scipy import ndimage
import numpy as np
import re

class GestoreRichieste:
    '''Gestore delle richieste:
    Soddisfa le i casi d'uso "Compra titolo", "Ricerca per immagine, 
    "Naviga risultati", "ricerca per informazione" e "Visualizza più 
    ricercati"
    '''
    def __init__(self,GestoreDB):
        '''Inizializzazzione della classe GestoreRichieste

        Args:
            gestoreDB (gestoreDB): Classe che si occupa di gestire il KG
        '''
        
        self.GestoreDB = GestoreDB
    
    
    def elabora_immagine(self, img):                                    		
        '''Elaborazione delle immagini

        Args:
            img (img): Immagine da analizzare

        Returns:
            libri (str): testo letto dalla foto

        Si occupa di fare del preprocessing sull'immagine passata 
        per poi estrapolarne il testo
        '''
        
        width, height, _ = img.shape
        if width > 5000 or height > 5000:
            self.scrivi_log("Ricevuta immagine troppo grande: %ix%i"%(width,height))
            return []
        elif width < 100 or height < 100:
            self.scrivi_log("Ricevuta immagine troppo piccola: %ix%i"%(width,height))
            return []
        
        libro = ""
        
        # Metodo 1: lettura naive
        testo = pytesseract.image_to_string(img,lang ='ita')
        testo = re.sub(r'[\n \t]+',' ', testo)
        testo = re.sub(r'[^A-Z a-z 0-9 \']+','', testo)
        testo = testo+' '
        libro += testo
        
        ########
        
        # Metodo 2: thresholding gaussiano
        img2 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img2 = cv2.adaptiveThreshold(img2,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                                     cv2.THRESH_BINARY,11,2)
        testo = pytesseract.image_to_string(img2,lang ='ita')
        testo = re.sub(r'[\n \t]+',' ', testo)
        testo = re.sub(r'[^A-Z a-z 0-9 \']+','', testo)
        testo = testo+' '
        libro += testo
        
        ########
        
        # Metodo 3: erosione e inversione maschera
        im3 = ndimage.binary_erosion(img2, structure=np.ones((5,3))).astype(np.float32)
        im3 = ndimage.binary_dilation(im3, structure=np.ones((5,3))).astype(np.float32)
        im3 = 1 - im3
        im3 = cv2.medianBlur(im3, 5)
        testo = pytesseract.image_to_string(im3)
        testo = re.sub(r'[\n \t]+',' ', testo)
        testo = re.sub(r'[^A-Z a-z 0-9 \']+','', testo)
        testo = testo+' '
        libro += testo
        
        libri = self.ricerca_libri("", "", libro)
        return libri
    
    
    def ricerca_libri_naive(self,testo,utente='0000'):
        '''Ricerca libri in modo naive

        Args:
            testo (string): testo che può essere il titolo del libro, il
                            il cognome dell'autore o il nome completo
                            dell'autore
            utente (string): argomento aggiuntivo

        Returns:
            libri (libro): lista di libri inerenti al libro da cercare

        Approccio a tentativi: prima cerca il libro come se il testo fosse il
        titolo del libro, se restituisce un libro allora cerco titoli simili da
        consigliare. Altrimenti provo cercando libri per nome completo
        dell'autore o solo cognome.
        '''
        
        libri = self.GestoreDB.ricerca_per_titolo(testo)

        if len(libri) > 0:
            self.GestoreDB.aggiorna_cronologia(testo,utente)
            libri.extend(self.GestoreDB.cerca_titoli_simili(testo))

        if len(libri) < 1:
            libri = self.GestoreDB.ricerca_per_autore(testo,"")

        if len(libri) < 1:
            libri = self.GestoreDB.ricerca_per_autore("",testo)

        return libri


    def ricerca_libri(self,nomeIntero,cognome,titolo,utente='0000'):
        '''Ricerca libri ottimizzata

        Args:
            nomeIntero (string): nome e cognome dell'autore
            cognome (string): cognome dell'autore
            titolo (string): titolo del libro
            utente (string): utente aggiuntivo

        Returns:
            libri (libro): lista di libri inerenti al libro da cercare/autore

        Cerca i titolo in base alle informazioni a disposizione. Avere
        nomeItero o cognome è indifferete, avere il nome dell'autore esclude la
        ricerca per titolo.
        '''
        
        if(len(cognome)>0 or len(nomeIntero)>0):
            libri = self.GestoreDB.ricerca_per_autore(nomeIntero,cognome)
        else:
            libri = self.GestoreDB.ricerca_per_titolo(titolo)
            if libri is not None:
                self.GestoreDB.aggiorna_cronologia(titolo,utente)
                libri.extend(self.GestoreDB.cerca_titoli_simili(titolo))
        return libri


    def ricerca_libro(self,titolo):
        '''Ricerca libro

        Args:
            titolo (string): titolo del libro

        Returns:
            libro (libro): libro cercato
        '''
        
        return  self.GestoreDB.ricerca_per_titolo(titolo)


    def compra_titolo(self, titolo):
        '''Compra titolo

        Args:
            titolo (string): titolo del libro

        Returns:
            link (string): link di acquisto generato

        Crea il link di acquisto per il titolo desiderato.
        '''
        
        if titolo in [""," ",",","?","'",'"']:
          self.scrivi_log("Errore nella generazione del link di acquisto: titolo == " + titolo)
          return None
        
        titolo = quote(titolo.decode("utf_8").encode('latin_1'),safe=':/')
        link = "https://www.amazon.it/s/ref=nb_sb_ss_c_1_14/258-9929431-0261\
715?__mk_it_IT=ÅMÅŽÕÑ&url=search-alias%3Daps&field-keywords="
        link = link + titolo.replace(" ","+")
        return link


    def crea_descrizione(self,libro,maxN):
        '''Crea la descrizione per ogni libro
        
        Args:
            libro(list): lista dove sifatta [titolo, autore, sunto]
            maxN(int): numero massimo di caratteri
            
        Returns:
            string: descrizione del libro

        Genera la descrizione del libro con al massimo maxN + n 
        caratteri, dove n sono i restanti caratteri dell'ultima parola
        troncata se maxN è -1 la descrizione non viene tagliata
        '''
        
        commento = libro[2]
        if(maxN != -1 and len(commento)>maxN):
            pos = commento[maxN:].find(' ')
            commento = commento[0:(maxN+pos)] + "..."
        return ("Titolo:  " + libro[0]+ "\nAutore:  " + libro[1] + "\n\n" + commento)
    
    
    def libri_in_classifica(self,nItem):
        '''Visualizza i libri in classifica

        Args:
            nItem (int): numero di titoli da visualizzare

        Returns:
            libri (libro): lista di libri nelle prime posizioni 
            in classifica

        Restituisce i primi nItem libri in classifica aggiornando le 
        varie liste ordinandole per frequenza
        '''
        
        self.GestoreDB.aggiorna_classifica()
        libri = []
        classifica = self.GestoreDB.classifica
        if len(classifica)>0:
            libri = self.GestoreDB.ricerca_per_titolo(classifica[0])
            nItem = min(nItem,len(classifica))
            for i in range(1,nItem):
                libri.extend(self.GestoreDB.ricerca_per_titolo(classifica[i]))
        return libri

    def scrivi_log(self, string):
        '''Scrive nel file di log un messaggio'''
        with open("log.txt", "a") as log:
             log.write(string + "\n")

