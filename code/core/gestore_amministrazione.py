#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rdflib         import Graph, URIRef
from SPARQLWrapper  import SPARQLWrapper, JSON
from urllib2        import quote
from PIL            import Image
from threading      import Thread
import argparse
import cv2
import pickle
import os
import signal
import time
import datetime

class GestoreAmministrazione(Thread):
    '''Gestore amministrazione:
    Soddisfa le i casi d'uso            #TODO
    '''
    
    def __init__(self,GestoreDB,directory=''):
        '''Inizializzazzione della classe gestoreAmministrazione

        Args:
            gestoreDB (gestoreDB) - Classe che si occupa di gestire il KG
            directory (str)       - stringa path directory

        Istanzia il gestore del KG e le statistiche

        classifica (list) - contiene i titoli ricercati
        rank (list)       - contiene le frequenze dei titoli ricercati
        '''
        
        Thread.__init__(self)
        self.GestoreDB = GestoreDB
        self.directory = directory
        self.statistiche = None
    
    
    def run(self):
        '''Si occupa di eseguire la funzione di backup
        
        Args:
            none
        
        Richiama la funzione backup_manuale ed informa dell'
        esecuzione avvenuta
        '''
        
        while True:
            time.sleep(2500000)
            self.backup_manuale()
            print(str(datetime.datetime.utcnow()) + " backup eseguito")
    
    
    def backup_manuale(self,path=None):                              						
        '''Creazione di file di backup											

        Args:
          path  - Directory in cui salvare i backup															

        Genera il backup dei diversi database, vengono fatti 3 bakcup:
            knowledge_graph.bak - backup del grafo
            utenti.bak          - backup degli utenti
            cronologia.bak      - backup delle ricerche effettuate dagli utenti
        '''
        
        if path is None:
            path = self.directory
        
        if not os.access(path, os.W_OK):
            if os.path.exists(path):
              self.scrivi_log("Errore nella scrittura dei file di backup: %s non è accessibile"%(path))
              print("Error: directory not accessible for writing")
              return
            else:
              try:
                os.makedirs(path)
              except:
                self.scrivi_log("Errore nella scrittura dei file di backup: %s non è accessibile"%(path))
                print("Error: directory not accessible for writing")
                return
            
        if len(self.GestoreDB.kg) > 0:
            name = 'knowledge_graph.bak'
            self.GestoreDB.kg.serialize(destination=path+name, format='turtle')
        
        if self.GestoreDB.utenti != []:
            name = 'utenti.bak'
            with open(path + name, 'wb') as f:
                pickle.dump(self.GestoreDB.utenti, f, pickle.HIGHEST_PROTOCOL)
        
        if self.GestoreDB.cronologia != dict(titolo=[],utente=[]):
            name = 'cronologia.bak'
            with open(path + name, 'wb') as f:
                pickle.dump(self.GestoreDB.cronologia, f, pickle.HIGHEST_PROTOCOL)




    def ripristina_backup(self,path=None):
        '''Si occupa di riprisinare i backup									

        Args:
            path  - directory dove ripristinare i backup
        
        Funzione che si occupa del ripristino dei 3 backup:
            knowledge_graph.bak - backup del grafo
            utenti.bak          - backup degli utenti
            cronologia.bak      - backup delle ricerche
        '''
        
        if path is None:
            path = self.directory
        
        if not os.access(path, os.R_OK):
            self.scrivi_log("Errore nella lettura dei file di backup: %s non è accessibile"%(path))
            print("Error: directory not accessible for reading")
            return
        
        name = 'knowledge_graph.bak'
        try:
            self.GestoreDB.kg.parse(path + name, format='turtle')
        except:
            print("Warning: no knowledge_graph.bak file found.")

        name = 'utenti.bak'
        try:
            with open(path + name, 'rb') as f:
                self.GestoreDB.utenti = pickle.load(f)
        except:
            print("Warning: no utenti.bak file found.")
        
        name = 'cronologia.bak'
        try:
            with open(path + name, 'rb') as f:
                self.GestoreDB.cronologia = pickle.load(f)
        except:
            print("Warning: no cronologia.bak file found.")


    def visualizza_statistiche(self):
        '''Visualizza statistiche

        Stampa a schermo:
          - numero di utenti
          - numero delle ricerche
          - numero di libri nel database
        '''
        
        n_ricerche = len(self.GestoreDB.cronologia['titolo'])
        n_utenti = len(self.GestoreDB.utenti)
        n_elementi = len(self.GestoreDB.kg)
        
        print("numero di utenti: %i"%(n_utenti))
        print("numero di ricerche effettuate: %i"%(n_ricerche))
        print("numero di libri nel database: %i"%(n_elementi))
    
    def scrivi_log(self, string):
        '''Scrive nel file di log un messaggio'''
        with open("log.txt", "a") as log:
             log.write(string + "\n")

