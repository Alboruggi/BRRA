#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import shutil
from rdflib import Graph
from core.gestore_DB import *
from core.gestore_amministrazione import *


class TestRipristinaBackup(unittest.TestCase):
  
  def setUp(self):
    self.gestoreDB = GestoreDB()
    self.gestoreAmministrazione = GestoreAmministrazione(self.gestoreDB)
  
  def tearDown(self):
    del self.gestoreDB
    del self.gestoreAmministrazione
    try:
      os.remove("log.txt")
    except: pass
  
  def test_RB_1(self):
    self.gestoreAmministrazione.ripristina_backup("/nodirectory/")
    self.assertEqual(self.gestoreDB.cronologia, dict(titolo=[],utente=[]))
    self.assertEqual(self.gestoreDB.utenti, [])
    self.assertEqual(len(self.gestoreDB.kg), 0)
  
  def test_RB_2(self):
    self.gestoreAmministrazione.ripristina_backup("/root/")
    self.assertEqual(self.gestoreDB.cronologia, dict(titolo=[],utente=[]))
    self.assertEqual(self.gestoreDB.utenti, [])
    self.assertEqual(len(self.gestoreDB.kg), 0)
  
  def test_RB_3(self):
    self.gestoreAmministrazione.ripristina_backup("testfiles/noback/")
    self.assertEqual(self.gestoreDB.cronologia, dict(titolo=[],utente=[]))
    self.assertEqual(self.gestoreDB.utenti, [])
    self.assertEqual(len(self.gestoreDB.kg), 0)
  
  def test_RB_4(self):
    self.gestoreAmministrazione.ripristina_backup("testfiles/back_u/")
    self.assertEqual(self.gestoreDB.cronologia, dict(titolo=[],utente=[]))
    self.assertNotEqual(self.gestoreDB.utenti, [])
    self.assertEqual(len(self.gestoreDB.kg), 0)
  
  def test_RB_5(self):
    self.gestoreAmministrazione.ripristina_backup("testfiles/back_cron/")
    self.assertNotEqual(self.gestoreDB.cronologia, dict(titolo=[],utente=[]))
    self.assertEqual(self.gestoreDB.utenti, [])
    self.assertEqual(len(self.gestoreDB.kg), 0)
  
  def test_RB_6(self):
    self.gestoreAmministrazione.ripristina_backup("testfiles/back_kg/")
    self.assertEqual(self.gestoreDB.cronologia, dict(titolo=[],utente=[]))
    self.assertEqual(self.gestoreDB.utenti, [])
    self.assertNotEqual(len(self.gestoreDB.kg), 0)
  
  def test_RB_7(self):
    self.gestoreAmministrazione.ripristina_backup("testfiles/back_ukg/")
    self.assertEqual(self.gestoreDB.cronologia, dict(titolo=[],utente=[]))
    self.assertNotEqual(self.gestoreDB.utenti, [])
    self.assertNotEqual(len(self.gestoreDB.kg), 0)
  
  def test_RB_8(self):
    self.gestoreAmministrazione.ripristina_backup("testfiles/back/")
    self.assertNotEqual(self.gestoreDB.cronologia, dict(titolo=[],utente=[]))
    self.assertNotEqual(self.gestoreDB.utenti, [])
    self.assertNotEqual(len(self.gestoreDB.kg), 0)


class TestBackupManuale(unittest.TestCase):
  
  def setUp(self):
    self.gestoreDB = GestoreDB()
    self.gestoreAmministrazione = GestoreAmministrazione(self.gestoreDB)
  
  def tearDown(self):
    del self.gestoreDB
    del self.gestoreAmministrazione
    try:
      shutil.rmtree("newback/")
    except: pass
    try:
      os.remove("log.txt")
    except: pass
  
  def test_BM_1(self):
    self.gestoreAmministrazione.backup_manuale("/nodirectory/")
  
  def test_BM_2(self):
    self.gestoreAmministrazione.backup_manuale("/root/")
  
  def test_BM_3(self):
    self.gestoreDB.ricerca_per_titolo("Moby Dick")
    self.gestoreAmministrazione.backup_manuale("newback/test3/")
    self.assertFalse(os.path.exists("newback/test3/cronologia.bak"))
    self.assertTrue(os.path.exists("newback/test3/knowledge_graph.bak"))
    self.assertFalse(os.path.exists("newback/test3/utenti.bak"))
  
  def test_BM_4(self):
    self.gestoreDB.inserisci_utente("8778867")
    self.gestoreDB.aggiorna_cronologia("Moby Dick", "8778867")
    self.gestoreAmministrazione.backup_manuale("newback/test4/")
    self.assertTrue(os.path.exists("newback/test4/cronologia.bak"))
    self.assertFalse(os.path.exists("newback/test4/knowledge_graph.bak"))
    self.assertTrue(os.path.exists("newback/test4/utenti.bak"))
  
  def test_BM_5(self):
    self.gestoreDB.inserisci_utente("8778867")
    self.gestoreDB.ricerca_per_titolo("Moby Dick")
    self.gestoreAmministrazione.backup_manuale("newback/test5/")
    self.assertFalse(os.path.exists("newback/test5/cronologia.bak"))
    self.assertTrue(os.path.exists("newback/test5/knowledge_graph.bak"))
    self.assertTrue(os.path.exists("newback/test5/utenti.bak"))
  
  def test_BM_6(self):
    self.gestoreAmministrazione.backup_manuale("newback/test6/")
    self.assertFalse(os.path.exists("newback/test6/cronologia.bak"))
    self.assertFalse(os.path.exists("newback/test6/knowledge_graph.bak"))
    self.assertFalse(os.path.exists("newback/test6/utenti.bak"))


if __name__ == "__main__":
  unittest.main()
