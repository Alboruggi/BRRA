[FUNZIONALI]

il sistema deve:
  poter analizzare un'immagine e ricercare e riconoscere testo all'interno
  
  poter correlare un libro ad una lista di libri che abbiano una qualche attinenza [definizione delle metriche di somiglianza]
  
  permettere agli utenti di ricercare libri:
    - per immagini
    - per titolo
    - per autore
  
  permettere agli utenti di visualizzare e navigare la cronologia delle ricerche
  
  proporre agli utenti un link ad uno store online per l'acquisto dei libri visualizzati
  
  poter ricercare informazioni riguardanti i titoli in internet [dbpedia]
  
  aggiornare il database locale delle informazioni sui libri ogni volta che trova o genera nuove informazioni da una ricerca
  
  permettere agli amministratori di far partire/fermare il servizio agli utenti [bot telegram]
  
  permettere agli admin di operare sul database dei titoli:
    - backup
    - ripristino backup
    FUTURE - merge da nuovo grafo locale
    - eliminazione database
  
  permettere agli admin di operare sul database degli utenti:
    - backup
    - ripristino backup
    - interrogazione su dati aggregati [statistiche]
    - eliminazione database
    - eliminazione dati singolo utente



[VINCOLI DI PROGETTAZIONE]
il sistema deve:
  
  poter essere configurato e fatto funzionare su un qualsiasi sistema che soddisfi le seguenti dipendenze:
    [lista delle dipendenze: python2.7 e relative librerie + altra roba se serve altra roba]



[NON FUNZIONALI]
il sistema deve:
  
  mantenere un database locale delle informazioni sui libri trovate in internet
  
  mantenere un database locale delle informazioni sulla navigazione degli utenti:
    - cronologia degli utenti
    - dati aggregati sulle ricerche degli utenti
  
  poter gestire diverse sessioni di diversi utenti contemporaneamente [quantificazione #sessioni]
  
  mantenere coerente il database dei titoli ogni volta che viene aggiornato [problema di archi mancanti nel merge di grafi]
  
  avere percentuali di miss nella ricerca dei titoli accettabili [quantificazione dei livelli di accettabilità]
  
  rispondere in tempi accettabili alle rechieste di utenti e/o amministratori oppure abortirle, notificare l'utente e registrare il fatto in un log[quantificazione dei livelli di accettabilità per ogni funzionalità]
  
  lasciare il sistema in uno stato coerente ogni volta che un'azione viene abortita



[ATTRIBUTI DI QUALITÀ DEL SOFTWARE]
il sistema deve essere suddiviso in package [descrizione della suddivisione dei package & spiagazione delle loro funzionalità]

ogni package che contiene funzioni di interfaccia pubblica (API) deve essere commentato secondo quanto specificato nella Python Developer's Guide all'interno della PEP 257

