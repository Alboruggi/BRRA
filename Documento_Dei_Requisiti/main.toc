\contentsline {section}{\numberline {1}Introduzione}{1}
\contentsline {subsection}{\numberline {1.1}Scopo del documento}{1}
\contentsline {subsection}{\numberline {1.2}Ambito applicativo}{1}
\contentsline {subsection}{\numberline {1.3}Definizioni, acronimi e abbreviazioni}{2}
\contentsline {subsection}{\numberline {1.4}Riferimenti}{3}
\contentsline {subsubsection}{\numberline {1.4.1}Telegram}{3}
\contentsline {subsubsection}{\numberline {1.4.2}DBpedia}{3}
\contentsline {subsection}{\numberline {1.5}Panoramica del documento}{4}
\contentsline {section}{\numberline {2}Descrizione generale}{5}
\contentsline {subsection}{\numberline {2.1}Prospetto del prodotto}{5}
\contentsline {subsubsection}{\numberline {2.1.1}Interfaccia utente}{7}
\contentsline {subsubsection}{\numberline {2.1.2}Interfacce software}{7}
\contentsline {subsection}{\numberline {2.2}Funzionalit\IeC {\`a} del software}{7}
\contentsline {subsubsection}{\numberline {2.2.1}Obiettivi funzionali di base}{7}
\contentsline {subsection}{\numberline {2.3}Caratteristiche degli utenti e scenari di utilizzo}{9}
\contentsline {subsubsection}{\numberline {2.3.1}Utenti del sistema}{9}
\contentsline {subsubsection}{\numberline {2.3.2}Scenari di utilizzo}{9}
\contentsline {subsection}{\numberline {2.4}Assunzioni e dipendenze}{11}
\contentsline {section}{\numberline {3}Requisiti specifici}{12}
\contentsline {subsection}{\numberline {3.1}Modello di dominio}{12}
\contentsline {subsection}{\numberline {3.2}Requisiti funzionali}{13}
\contentsline {subsubsection}{\numberline {3.2.1}Analizza immagine}{13}
\contentsline {subsubsection}{\numberline {3.2.2}Analizza testo}{13}
\contentsline {subsubsection}{\numberline {3.2.3}Ricerca libro}{13}
\contentsline {subsubsection}{\numberline {3.2.4}Ricerca libri correlati}{14}
\contentsline {subsubsection}{\numberline {3.2.5}Backup giornaliero}{14}
\contentsline {subsubsection}{\numberline {3.2.6}Ripristino avvio}{15}
\contentsline {subsubsection}{\numberline {3.2.7}Memorizza cronologia}{15}
\contentsline {subsubsection}{\numberline {3.2.8}Navigazione cronologia}{15}
\contentsline {subsubsection}{\numberline {3.2.9}Ricerca libri pi\IeC {\`u} ricercati}{16}
\contentsline {subsubsection}{\numberline {3.2.10}Crea link di acquisto}{16}
\contentsline {subsubsection}{\numberline {3.2.11}Integrazione informazioni}{17}
\contentsline {subsubsection}{\numberline {3.2.12}Gestore servizio amministratori}{17}
\contentsline {subsubsection}{\numberline {3.2.13}Backup titoli}{17}
\contentsline {subsubsection}{\numberline {3.2.14}Ripristino titoli}{18}
\contentsline {subsubsection}{\numberline {3.2.15}Backup utenti}{18}
\contentsline {subsubsection}{\numberline {3.2.16}Ripristino utenti}{18}
\contentsline {subsubsection}{\numberline {3.2.17}Elimina database utenti}{19}
\contentsline {subsubsection}{\numberline {3.2.18}Visualizza statistiche}{19}
\contentsline {subsection}{\numberline {3.3}Requisiti interfaccia esterna}{20}
\contentsline {subsection}{\numberline {3.4}Vincoli di progettazione}{20}
\contentsline {subsection}{\numberline {3.5}Requisiti non funzionali}{20}
\contentsline {subsubsection}{\numberline {3.5.1}Requisiti di performance}{20}
\contentsline {subsubsection}{\numberline {3.5.2}Attributi di qualit\IeC {\`a} del software}{21}
