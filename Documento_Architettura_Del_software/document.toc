\contentsline {section}{\numberline {1}Introduzione}{1}
\contentsline {section}{\numberline {2}Architettura del problema}{2}
\contentsline {subsection}{\numberline {2.1}Casi d'uso}{2}
\contentsline {subsection}{\numberline {2.2}Modello dei Dati}{3}
\contentsline {subsection}{\numberline {2.3}Diagrammi delle attivit\IeC {\`a}}{4}
\contentsline {subsubsection}{\numberline {2.3.1}Ricerca per immagine}{5}
\contentsline {subsubsection}{\numberline {2.3.2}Ricerca per informazione}{5}
\contentsline {subsubsection}{\numberline {2.3.3}Ricerca database info}{6}
\contentsline {subsubsection}{\numberline {2.3.4}Compra titolo}{7}
\contentsline {subsubsection}{\numberline {2.3.5}Visualizza pi\IeC {\`u} ricercati}{7}
\contentsline {subsubsection}{\numberline {2.3.6}Aggiorna pi\IeC {\`u} ricercati}{8}
\contentsline {subsubsection}{\numberline {2.3.7}Backup automatico}{8}
\contentsline {subsubsection}{\numberline {2.3.8}Backup manuale}{9}
\contentsline {subsubsection}{\numberline {2.3.9}Ripristina backup}{9}
\contentsline {subsubsection}{\numberline {2.3.10}Visualizza statistiche}{10}
\contentsline {section}{\numberline {3}Architettura logica}{11}
\contentsline {subsection}{\numberline {3.1}Partizionamento delle funzionalit\IeC {\`a}}{11}
\contentsline {subsubsection}{\numberline {3.1.1}Gestore amministrazione}{12}
\contentsline {subsubsection}{\numberline {3.1.2}Gestore richieste}{13}
\contentsline {subsubsection}{\numberline {3.1.3}Gestore DB}{14}
\contentsline {subsection}{\numberline {3.2}Footprint del partizionamento}{15}
\contentsline {section}{\numberline {4}Architettura Concreta}{16}
\contentsline {subsection}{\numberline {4.1}Diagrammi di sequenza}{16}
\contentsline {subsubsection}{\numberline {4.1.1}Ricerca per immagine}{16}
\contentsline {subsubsection}{\numberline {4.1.2}Ricerca per informazione}{17}
\contentsline {subsubsection}{\numberline {4.1.3}Ricerca database info}{17}
\contentsline {subsubsection}{\numberline {4.1.4}Compra titolo}{18}
\contentsline {subsubsection}{\numberline {4.1.5}Visualizza pi\IeC {\`u} ricercati}{18}
\contentsline {subsubsection}{\numberline {4.1.6}Aggiorna pi\IeC {\`u} ricercati}{19}
\contentsline {subsubsection}{\numberline {4.1.7}Backup automatico}{19}
\contentsline {subsubsection}{\numberline {4.1.8}Backup manuale}{20}
\contentsline {subsubsection}{\numberline {4.1.9}Ripristina backup}{20}
\contentsline {subsubsection}{\numberline {4.1.10}Visualizza statistiche}{21}
\contentsline {section}{\numberline {5}Architettura di Deployment}{22}
\contentsline {subsection}{\numberline {5.1}Diagramma di Deployment}{22}
